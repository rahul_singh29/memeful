//
//  Token.swift
//  Memeful
//
//  Created by Rahul Singh on 18/12/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation


struct Token: Codable {
    var accessToken: String?
    var accountUsername: String?
    var expiresIn: Int?
    var refreshToken: String?
    var scope: String?
    var tokenType: String?
    
    private enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case accountUsername = "account_username"
        case expiresIn = "expires_in"
        case refreshToken = "refresh_token"
        case scope = "scope"
        case tokenType = "token_type"
    }
}
