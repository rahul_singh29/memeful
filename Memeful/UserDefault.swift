//
//  UserDefault.swift
//  Memeful
//
//  Created by Rahul Singh on 18/12/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation


class UserDefaults {
    let refreshToken = UserDefaultValue<String>(key: "refreshToken")
    let accessToken = UserDefaultValue<String>(key: "access_token")

    
}


class UserDefaultValue<T: UserDefaultsStorable> {
    fileprivate let key: String
    fileprivate(set) var value: T?
    fileprivate let userDefaults: Foundation.UserDefaults
    
    init(key: String, userDefaults: Foundation.UserDefaults = Foundation.UserDefaults.standard) {
        self.key = key
        self.value = T.load(key, userDefaults: userDefaults)
        self.userDefaults = userDefaults
    }
    
    func update(_ newValue: T) {
        value = newValue
        newValue.save(key, userDefaults: userDefaults)
    }
    
    func reset() {
        userDefaults.removeObject(forKey: key)
    }
}

class InitialUserDefaultValue<T: UserDefaultsStorable> {
    fileprivate let key: String
    fileprivate(set) var value: T
    fileprivate let initialValue: T
    fileprivate let userDefaults: Foundation.UserDefaults
    
    init(key: String, initialValue: T, userDefaults: Foundation.UserDefaults = Foundation.UserDefaults.standard) {
        self.key = key
        self.value = T.load(key, userDefaults: userDefaults) ?? initialValue
        self.initialValue = initialValue
        self.userDefaults = userDefaults
    }
    
    func update(_ newValue: T) {
        value = newValue
        value.save(key, userDefaults: userDefaults)
    }
    
    func reset() {
        initialValue.save(key, userDefaults: userDefaults)
    }
}

protocol UserDefaultsStorable {
    func save(_ key: String, userDefaults: Foundation.UserDefaults)
    static func load(_ key: String, userDefaults: Foundation.UserDefaults) -> Self?
}

// MARK: Concretes
extension String: UserDefaultsStorable {
    func save(_ key: String, userDefaults: Foundation.UserDefaults) {
        userDefaults.set(self, forKey: key)
    }
    
    static func load(_ key: String, userDefaults: Foundation.UserDefaults) -> String? {
        return userDefaults.string(forKey: key)
    }
}

extension Bool: UserDefaultsStorable {
    func save(_ key: String, userDefaults: Foundation.UserDefaults) {
        userDefaults.set(self, forKey: key)
    }
    
    static func load(_ key: String, userDefaults: Foundation.UserDefaults) -> Bool? {
        return userDefaults.bool(forKey: key)
    }
}
