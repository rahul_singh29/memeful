//
//  LoginViewController.swift
//  Memeful
//
//  Created by Rahul Singh on 16/12/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import UIKit
import WebKit


class LoginViewController: UIViewController {
    
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let myURLString = "https://api.imgur.com/oauth2/authorize?client_id=2321328b8d28906&response_type=code"
        let request = URLRequest(url: URL(string: myURLString)!)
        webView = WKWebView(frame: self.view.frame)
        self.view.addSubview(webView)
        webView.navigationDelegate = self
        webView.load(request)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}
extension LoginViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString, urlStr.contains("imemes://app") {
            let code = urlStr.components(separatedBy: "code=")[1]
            self.getToken(from: code)
        }
        decisionHandler(.allow)
    }
    
    private func getToken(from code:String) {
        let params = GetTokenParameter(refreshToken: code, grantType: "authorization_code")
        ApiClient.shared.getToken(params: params, handler: RegularRequestCompletion<Token>(success: { (response) in
            print(response)
        }, failure: { (error) in
            print(error)
        }))
    }
    
    private func getAccount() {
        ApiClient.shared.getAccount(handler: RegularRequestCompletion<Token>(success: { (response) in
            
        }, failure: { (error) in
            
        }))
    }
}
