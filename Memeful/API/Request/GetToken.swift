//
//  FactsRequest.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation
import Alamofire

extension ApiClient {
    
    func getToken(params: RequestParameters, handler: RegularRequestCompletion<Token>) {
        request(method: .post, path: "/oauth2/token", parameters: params, handler: RequestCompletion.regular(completion: handler))
    }
    
}


struct GetTokenParameter: RequestParameters {
    let refreshToken: String
    let grantType: String
    
    func parameters() -> [String : Any] {
        return [
            "code": refreshToken,
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
            "grant_type": grantType
        ]
    }
}
