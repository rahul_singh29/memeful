//
//  GetAccount.swift
//  Memeful
//
//  Created by Rahul Singh on 18/12/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation
import Alamofire

extension ApiClient {
    
    func getAccount(params: RequestParameters, handler: RegularRequestCompletion<Token>) {
        request(path: "/account/me", parameters: params, handler: RequestCompletion.regular(completion: handler))
    }
    
}
