//
//  ApiClient.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation
import Alamofire
import UIKit


class ApiClient: SessionManager {
    static let shared = ApiClient(configuration: ApiClient.urlSessionConfiguration(), serverTrustPolicyManager: nil)
    static func urlSessionConfiguration() -> URLSessionConfiguration {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.httpCookieStorage = nil
        configuration.httpCookieAcceptPolicy = .never
        configuration.httpShouldSetCookies = false
        return configuration
    }
    
    static var apiDomain = "https://api.imgur.com"
    
    
    func request<T: Codable>(method: Alamofire.HTTPMethod = .get, path: String, parameters: RequestParameters? = nil, encoding: ParameterEncoding = JSONEncoding(), handler: RequestCompletion<T>) {
        
        // Send request
        let endpointURI = ApiClient.apiDomain + path
        let request = super.request(endpointURI, method: method, parameters: parameters?.parameters(), encoding: encoding, headers: ApiHeaders.defaultHeaders())

        request.responseData(completionHandler: { (response) -> Void in
            switch response.result {
            case .success(let data):
                let decoder = JSONDecoder()
                do {
                    print(response)
                        let responseObject = try decoder.decode(T.self, from: data)
                        switch handler {
                        case .regular(let completion): completion.success(responseObject)
                        }
                } catch let error {
                     return handler.failure!(NSError.errorWithMessage(error.localizedDescription))
                }
            case .failure:
                handler.failure?(NSError.errorWithMessage(""))
            }
        })
        debugPrint(request)
    }
}

protocol RequestParameters {
    func parameters() -> [String : Any]
}

struct ApiHeaders {
    static func defaultHeaders() -> [String: String] {
        guard let accessToken = UserDefaults().accessToken.value, accessToken != "" else {
            return [:]
        }
        return ["Accept": "application/json", "Authorization" : "Bearer \(accessToken)"]
    }
}
