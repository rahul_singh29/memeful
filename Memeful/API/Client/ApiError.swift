//
//  ApiError.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation


extension NSError {
    
    static func errorWithMessage(_ message: String) -> NSError {
        return NSError(domain: "AppDomain", code: 0, userInfo: [NSLocalizedDescriptionKey : message])
    }
    
    static func errorWithDefaultMessage() -> NSError {
        return NSError(domain: "AppDomain", code: 0, userInfo: [NSLocalizedDescriptionKey : "Something went wrong"])
    }
}
